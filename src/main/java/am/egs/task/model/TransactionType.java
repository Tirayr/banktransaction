package am.egs.task.model;

public enum TransactionType {
    TRANSFER,
    CASHOUT,
    AMOUNT_EDDITION
}

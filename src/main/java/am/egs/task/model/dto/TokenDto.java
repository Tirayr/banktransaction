package am.egs.task.model.dto;

import java.io.Serializable;

public class TokenDto implements Serializable {

    private final String token;

    public TokenDto() {
        this.token = null;
    }

    public TokenDto(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}

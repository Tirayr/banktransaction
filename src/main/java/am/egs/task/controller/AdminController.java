package am.egs.task.controller;

import am.egs.task.model.entity.BankAccount;
import am.egs.task.service.BankAccountService;
import am.egs.task.util.exception.DublicatBankAccountException;
import am.egs.task.util.exception.EmptyBankAccountException;
import am.egs.task.util.exception.UserNotFoundException;
import am.egs.task.util.exception.WrongCodeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

@RestController
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    private BankAccountService bankAccountService;

    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);


    /*
     * add account
     * */
    @Transactional
    @PostMapping(value = "/createAccount")
    public ResponseEntity registration(@RequestHeader("token") String token,
                                       @RequestBody BankAccount bankAccount) throws EmptyBankAccountException, DublicatBankAccountException, MessagingException {
        logger.info("New account", bankAccount);
        bankAccountService.add(bankAccount);
        return ResponseEntity.ok().build();
    }


    /*
     *    verify
     * */
    @Transactional
    @PostMapping("/verify")
    public ResponseEntity verify(@RequestParam("email") String accountOwnerEmail,
                                 @RequestParam("code") String code) throws WrongCodeException, UserNotFoundException {
        bankAccountService.verify(accountOwnerEmail, code);
        return ResponseEntity.ok().build();

    }

}

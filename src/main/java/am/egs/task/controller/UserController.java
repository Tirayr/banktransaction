package am.egs.task.controller;

import am.egs.task.config.jwt.JwtProvider;
import am.egs.task.model.dto.TokenDto;
import am.egs.task.model.entity.Transaction;
import am.egs.task.model.entity.User;
import am.egs.task.service.UserService;
import am.egs.task.util.exception.EmptyBankAccountException;
import am.egs.task.util.exception.UserDublicateException;
import am.egs.task.util.exception.UserNotFoundException;
import am.egs.task.util.exception.WrongRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);


    /*
     *  registration
     * */
    @PostMapping(value = "/signUp")
    public ResponseEntity registration(@Valid @RequestBody User user) throws UserNotFoundException, UserDublicateException, MessagingException {
        logger.info("New User", user);
        userService.add(user);
        return ResponseEntity.ok().build();
    }


    /*
     *   login
     * */
    @PostMapping(value = "/signIn")
    public TokenDto login(@RequestParam("email") String email,
                          @RequestParam("password") String password) throws UserNotFoundException {

        logger.info("getting token via email and. And sign in with email and password");

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        User user = userService.getByEmail(email);
        String token = userService.getToken(email, user.getRole());
        TokenDto tokenDto = new TokenDto(token);

        return tokenDto;
    }

    /*
     *   get user via token
     * */
    @GetMapping("/signIn/getUser")
    public ResponseEntity getUser(@RequestHeader("token") String token) {
        logger.info("geting user via token", token);
        User user = userService.getUserByToken(token);
        return ResponseEntity.ok(user);
    }


    /*
     * transfer money
     * */
    @PostMapping(value = "/transfer")
    public synchronized ResponseEntity transferMoney(@RequestHeader("token") String token,
                                                     @RequestParam("accountNumber") String accountNumber,
                                                     @RequestParam("amount") double transferAmount) throws EmptyBankAccountException, UserNotFoundException, WrongRequestException {
        userService.transferMoney(token, transferAmount, accountNumber);
        return ResponseEntity.ok().build();
    }

    /*
     * cashout from amount
     * */
    @PostMapping(value = "/cashout")
    public synchronized ResponseEntity cashOutMoney(@RequestHeader("token") String token,
                                                    @RequestParam("accountNumber") String accountNumber,
                                                    @RequestParam("amount") double cashoutAmount) throws EmptyBankAccountException, UserNotFoundException, WrongRequestException {
        userService.cashoutMoney(token, cashoutAmount, accountNumber);
        return ResponseEntity.ok().build();
    }

    /*
     * amount eddition  money
     * */
    @PostMapping(value = "/amountEddition")
    public synchronized ResponseEntity amountEddition(@RequestHeader("token") String token,
                                                      @RequestParam("accountNumber") String accountNumber,
                                                      @RequestParam("amount") double amountEddition) throws EmptyBankAccountException, UserNotFoundException, WrongRequestException {
        userService.amountEddition(token, amountEddition, accountNumber);
        return ResponseEntity.ok().build();
    }

    /*
     *   show transaction list
     * */
    @GetMapping(value = "/transactionList")
    public List<Transaction> tansactionList(@RequestHeader("token") String token) throws WrongRequestException {
        List<Transaction> myTransactionList = userService.getTransactionList(token);
        return myTransactionList;
    }


}

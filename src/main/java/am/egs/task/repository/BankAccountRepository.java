package am.egs.task.repository;

import am.egs.task.model.entity.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    BankAccount getAccountByAccountNumber(String accountNumber);

    @Query(value = "SELECT * FROM task.bank_account WHERE bank_account.account_owner_email = :account_owner_email",
            nativeQuery = true)
    List<BankAccount> getAccountListByAccountOwnerEmail(@Param("account_owner_email") String account_owner_email);

    BankAccount getAccountByAccountOwnerEmail(String accountOwnerEmail);
}

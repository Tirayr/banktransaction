package am.egs.task.repository;

import am.egs.task.model.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query(value = "SELECT * FROM task.transaction WHERE task.transaction.account_owner_email = :account_owner_email",
            nativeQuery = true)
    List<Transaction> getTransactionListByEmail(@Param("account_owner_email")String email);

//
//    @Query(value = "SELECT * FROM task.bank_account WHERE bank_account.account_owner_email = :account_owner_email",
//            nativeQuery = true)
//    List<BankAccount> getAccountListByAccountOwnerEmail(@Param("account_owner_email") String account_owner_email);
}

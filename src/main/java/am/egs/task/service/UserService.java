package am.egs.task.service;


import am.egs.task.model.entity.Transaction;
import am.egs.task.model.entity.User;
import am.egs.task.util.exception.EmptyBankAccountException;
import am.egs.task.util.exception.UserDublicateException;
import am.egs.task.util.exception.UserNotFoundException;
import am.egs.task.util.exception.WrongRequestException;

import javax.mail.MessagingException;
import java.util.List;

public interface UserService {
    User getByEmail(String email) throws UserNotFoundException;

    void add(User user) throws UserNotFoundException, UserDublicateException, MessagingException;

    String getToken(String email, String role);

    User getUserByToken(String token);

    void transferMoney(String token, double transferAmount, String accountNumber) throws UserNotFoundException, EmptyBankAccountException, WrongRequestException;

    void cashoutMoney(String token, double cashoutAmount, String accountNumber) throws EmptyBankAccountException, UserNotFoundException, WrongRequestException;

    void amountEddition(String token, double amountEddition, String accountNumber) throws EmptyBankAccountException, UserNotFoundException;

    List<Transaction> getTransactionList(String token) throws WrongRequestException;
}

package am.egs.task.service.impl;

import am.egs.task.model.entity.BankAccount;


import am.egs.task.repository.BankAccountRepository;

import am.egs.task.service.BankAccountService;
import am.egs.task.util.GenerateCode;
import am.egs.task.util.MailSenderClient;
import am.egs.task.util.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;




    @Autowired
    private MailSenderClient mailSenderClient;


    /*
    *  create new bank account
    * */
    @Override
    public void add(BankAccount bankAccount) throws EmptyBankAccountException, DublicatBankAccountException, MessagingException {

        if (bankAccount == null){
            throw new EmptyBankAccountException("state is empty !!!");
        }

        if (bankAccountRepository.getAccountByAccountNumber(bankAccount.getAccountNumber()) != null){
            throw new DublicatBankAccountException("there is Account withis account number !!!");
        }

        String accountnumber = String.valueOf(bankAccount.getId() + bankAccount.getAccountOwnerEmail().hashCode());
        bankAccount.setAccountNumber(accountnumber);

        bankAccount.setVerficationCode(GenerateCode.generateCode());
        mailSenderClient.send(bankAccount.getAccountOwnerEmail(), "VerificationMessage", GenerateCode.getVerificationMessage(bankAccount));

        bankAccountRepository.save(bankAccount);

    }

    /*
    *  verify
    * */
    @Override
    public void verify(String accountOwnerEmail, String code) throws UserNotFoundException, WrongCodeException {
        BankAccount bankAccount = bankAccountRepository.getAccountByAccountOwnerEmail(accountOwnerEmail);

        if (bankAccount == null) {
            throw new UserNotFoundException("There is no user with this email !!!");
        }
        if (!bankAccount.getVerficationCode().equals(code)) {
            throw new WrongCodeException("Wrong verification code !!!");
        }
        bankAccount.setVerficationCode(null);
        bankAccountRepository.save(bankAccount);
    }
}

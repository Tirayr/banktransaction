package am.egs.task.service.impl;


import am.egs.task.config.jwt.JwtProvider;
import am.egs.task.model.TransactionType;
import am.egs.task.model.entity.BankAccount;
import am.egs.task.model.entity.Transaction;
import am.egs.task.model.entity.User;
import am.egs.task.repository.BankAccountRepository;
import am.egs.task.repository.TransactionRepository;
import am.egs.task.repository.UserRepository;
import am.egs.task.service.UserService;
import am.egs.task.util.TransactionsComparator;
import am.egs.task.util.exception.EmptyBankAccountException;
import am.egs.task.util.exception.UserDublicateException;
import am.egs.task.util.exception.UserNotFoundException;
import am.egs.task.util.exception.WrongRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private JwtProvider jwtProvider;


    @Autowired
    private TransactionRepository transactionRepository;


    @Override
    public User getByEmail(String email) throws UserNotFoundException {
        User user = userRepository.getUserByEmail(email);
        if (user == null) {
            throw new UserNotFoundException("There is no user with this email");
        } else
            return user;
    }

    /*
     *   Add user
     * */
    @Transactional
    @Override
    public void add(User user) throws UserNotFoundException, UserDublicateException, MessagingException {
        if (user == null) {
            throw new UserNotFoundException("User is empty");
        }

        if (userRepository.getUserByEmail(user.getEmail()) != null) {
            throw new UserDublicateException("there is user with this email");
        }

        user.setRole("ROLE_USER");

        List<BankAccount> bankAccounts = bankAccountRepository.getAccountListByAccountOwnerEmail(user.getEmail());
        user.setBankAccountList(bankAccounts);

        userRepository.save(user);
    }

    /*
     *  Get Token By Email and Roles
     * */
    @Override
    public String getToken(String email, String role) {
        String token = jwtProvider.createToken(email, role);
        return token;
    }

    /*
     *  Get User By Token
     * */
    @Override
    public User getUserByToken(String token) {
        String email = jwtProvider.getEmail(token);
        User user = userRepository.getUserByEmail(email);

        String role = user.getRole();

        if (role.equals("ROLE_ADMIN")) {
            return null;
        } else
            System.out.println(user.toString());
        return user;

    }

    @Transactional
    @Override
    public void transferMoney(String token, double transferAmount, String accountNumber) throws UserNotFoundException, EmptyBankAccountException, WrongRequestException {
        BankAccount bankAccount = chechkAccount(token, accountNumber);
        if (bankAccount.getAmount() < transferAmount) {
            throw new WrongRequestException("tranfer amount is more well than amount in your account");
        } else {
            double newAmount = bankAccount.getAmount() - transferAmount;
            bankAccount.setAmount(newAmount);
            bankAccountRepository.save(bankAccount);

            Transaction usersTransaction = new Transaction();
            LocalDateTime current = LocalDateTime.now();
            usersTransaction.setTransactionDate(current);
            usersTransaction.setAccountOwnerEmail(bankAccount.getAccountOwnerEmail());
            usersTransaction.setAccountNumber(accountNumber);
            usersTransaction.setTransactionType(TransactionType.TRANSFER);
            usersTransaction.setAmount(transferAmount);
            transactionRepository.save(usersTransaction);
        }
    }

    @Transactional
    @Override
    public void cashoutMoney(String token, double cashoutAmount, String accountNumber) throws EmptyBankAccountException, UserNotFoundException, WrongRequestException {
        BankAccount bankAccount = chechkAccount(token, accountNumber);
        if (bankAccount.getAmount() < cashoutAmount) {
            throw new WrongRequestException("cashout amount is more well than amount in your account");
        } else {
            bankAccount.setAmount(bankAccount.getAmount() - cashoutAmount);
            bankAccountRepository.save(bankAccount);

            Transaction usersTransaction = new Transaction();
            LocalDateTime current = LocalDateTime.now();
            usersTransaction.setTransactionDate(current);
            usersTransaction.setAccountOwnerEmail(bankAccount.getAccountOwnerEmail());
            usersTransaction.setAccountNumber(accountNumber);
            usersTransaction.setTransactionType(TransactionType.CASHOUT);
            usersTransaction.setAmount(cashoutAmount);
            transactionRepository.save(usersTransaction);
        }

    }

    @Transactional
    @Override
    public void amountEddition(String token, double amountEddition, String accountNumber) throws EmptyBankAccountException, UserNotFoundException {
        BankAccount bankAccount = chechkAccount(token, accountNumber);
        bankAccount.setAmount(bankAccount.getAmount() + amountEddition);
        bankAccountRepository.save(bankAccount);

        Transaction usersTransaction = new Transaction();
        LocalDateTime current = LocalDateTime.now();
        usersTransaction.setTransactionDate(current);
        usersTransaction.setAccountOwnerEmail(bankAccount.getAccountOwnerEmail());
        usersTransaction.setAccountNumber(accountNumber);
        usersTransaction.setTransactionType(TransactionType.AMOUNT_EDDITION);
        usersTransaction.setAmount(amountEddition);
        transactionRepository.save(usersTransaction);

    }

    @Override
    public List<Transaction> getTransactionList(String token) throws WrongRequestException {
        String email = jwtProvider.getEmail(token);
        if (email == null){
            throw new WrongRequestException("wrong account");
        }
        List<Transaction> transactions = transactionRepository.getTransactionListByEmail(email);
        if (transactions.isEmpty()){
            return null;
        } else {
            Collections.sort(transactions, new TransactionsComparator());
            return transactions;
        }
    }

    private BankAccount chechkAccount(String token, String accountNumber) throws UserNotFoundException, EmptyBankAccountException {
        String email = jwtProvider.getEmail(token);
        User user = userRepository.getUserByEmail(email);

        if (user == null) {
            throw new UserNotFoundException("there is nor user with this email");
        }

        List<BankAccount> bankAccounts = user.getBankAccountList();
        List<String> accountNumbers = new ArrayList<>();
        for (int i = 0; i < bankAccounts.size(); i++) {
            accountNumbers.add(bankAccounts.get(i).getAccountNumber());
        }

        if (accountNumbers.contains(accountNumber)) {
            BankAccount bankAccount = bankAccountRepository.getAccountByAccountNumber(accountNumber);
            return bankAccount;
        } else
            throw new EmptyBankAccountException("there is no account with this account number");
    }


}

package am.egs.task.service;

import am.egs.task.model.entity.BankAccount;
import am.egs.task.util.exception.DublicatBankAccountException;
import am.egs.task.util.exception.EmptyBankAccountException;
import am.egs.task.util.exception.UserNotFoundException;
import am.egs.task.util.exception.WrongCodeException;

import javax.mail.MessagingException;

public interface BankAccountService {
    void add(BankAccount bankAccount) throws EmptyBankAccountException, DublicatBankAccountException, MessagingException;

    void verify(String accountOwnerEmail, String code) throws UserNotFoundException, WrongCodeException;
}

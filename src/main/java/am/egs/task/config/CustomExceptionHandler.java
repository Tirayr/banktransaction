package am.egs.task.config;

import am.egs.task.model.dto.ErrorDto;

import am.egs.task.util.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value
            = {DublicatBankAccountException.class})
    protected ResponseEntity<Object> handleDublicatBankAccountException(DublicatBankAccountException ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorDto error = new ErrorDto(" There is already a account with this email", details);
        return new ResponseEntity(error, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value
            = {EmptyBankAccountException.class})
    protected ResponseEntity<Object> handleEmptyBankAccountException(EmptyBankAccountException ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorDto error = new ErrorDto(" There are not exist this account", details);
        return new ResponseEntity(error, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value
            = {WrongCodeException.class})
    protected ResponseEntity<Object> handleWrongCodeException(WrongCodeException ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorDto error = new ErrorDto("wrong verification code", details);
        return new ResponseEntity(error, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value
            = {UserNotFoundException.class})
    protected ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorDto error = new ErrorDto("User Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler(value
            = {UserDublicateException.class})
    protected ResponseEntity<Object> handleUserDublicateException(UserDublicateException ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorDto error = new ErrorDto("there is user with this email", details);
        return new ResponseEntity(error, HttpStatus.CONFLICT);
    }



    @ExceptionHandler(value
            = {WrongRequestException.class})
    protected ResponseEntity<Object> handleWrongRequestException(WrongRequestException ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorDto error = new ErrorDto("increment amount is more well than amount in your account", details);
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }

}

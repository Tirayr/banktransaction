package am.egs.task.util.exception;

public class WrongCodeException extends Throwable {

    public WrongCodeException(String message) {
        super(message);
    }

}

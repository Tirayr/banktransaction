package am.egs.task.util.exception;

public class WrongRequestException extends  Exception {
    public WrongRequestException(String message) {
        super(message);
    }

}

package am.egs.task.util.exception;

public class EmptyBankAccountException extends Exception {

    public EmptyBankAccountException(String message){
        super(message);
    }

}

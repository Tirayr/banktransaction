package am.egs.task.util;

import am.egs.task.model.entity.Transaction;

import java.util.Comparator;

public class TransactionsComparator implements Comparator<Transaction>{

    @Override
    public int compare(Transaction o1, Transaction o2) {
        return o1.getTransactionDate().compareTo(o2.getTransactionDate());
    }

}

package am.egs.task.util;

import am.egs.task.model.entity.BankAccount;


import java.util.Random;

public class GenerateCode {

    private static final char[] NUMBERS = "0123456789".toCharArray();

    public static String generateCode(){
        int count = 7;
        StringBuilder builder = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < count ; i++) {
            char c = NUMBERS[random.nextInt(NUMBERS.length)];
            builder.append(c);
        }
        return builder.toString();
    }

    public static String getVerificationMessage(BankAccount bankAccount){
        return "Hello dear " + bankAccount.getAccountOwnerName().substring(0,1).toUpperCase() +
                bankAccount.getAccountOwnerName().substring(1) +
                " You were successfully registered. Please verify. Your Verification code is: " + bankAccount.getVerficationCode();
    }

}
